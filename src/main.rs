extern crate tuple_storage;
extern crate nom_sql;
#[cfg(test)]
extern crate tempfile;

use std::borrow::Borrow;
use std::collections::HashMap;
use std::fs::{self, OpenOptions};
use std::io::{self, Read, Write};
use std::path::{Path, PathBuf};

use nom_sql::{*, SqlQuery::*, Literal::Integer, ConditionExpression::{Base, ComparisonOp}, Operator::*, ConditionBase::{Field}, };
use tuple_storage::{Storage, Tuple};

fn main() {
    let db_dir = "./target/demo.depot/";
    println!("Loading Tables from {}!", db_dir);

    // TODO open all the existing tables
    let mut db = DataBase::open_dir(db_dir).unwrap();

    let stdin = io::stdin();
    //let mut stdin = stdin.lock();
    loop {
        // process query
        print!("> ");
        flush_stdout();
        let mut buffer = String::new();
        stdin.read_line(&mut buffer).unwrap();
        //println!("{:?}", buffer);
        if buffer == "" {
            break;
        }
        let query = nom_sql::parse_query(&buffer);
        // println!("{:#?}", query);

        // execute query
        match query {
            Err(s) => println!("{}\n", s),
            Ok(query) => db.execute_query(&query),
        }
    }
}

pub struct DataBase {
    base_path: PathBuf,
    tables: HashMap<String, Storage>,
}

impl DataBase {
    pub fn open_dir(db_dir: &str) -> io::Result<DataBase> {
        let base_path = Path::new(db_dir).to_path_buf();
        match base_path.metadata() {
            Ok(metadata) =>
                if metadata.is_dir() == false {
                    println!("creating db_dir? {}", metadata.is_dir());
                },
            Err(_) => {
                println!("creating db_dir");
                fs::create_dir_all(&base_path)?
            },
        }
        let tables = fs::read_dir(&base_path)?
            .filter_map(|entry| entry.ok())
            .map(|file_path| {
                let file = OpenOptions::new()
                            .read(true).write(true).create(true)
                            .open(&file_path.path()).unwrap();
                let s = Storage::open(file).unwrap();

                (file_path.file_name().into_string().unwrap(), s)
            })
            .collect();

        println!("ready! \n");

        Ok(DataBase {
            base_path,
            tables,
        })
    }
    pub fn execute_query(&mut self, query: &SqlQuery) {
        let mk_filter = |s: &Storage, where_clause: &_| {
            match where_clause {
                Some(ref where_clause) => {
                    match where_clause {
                        ComparisonOp(ConditionTree { operator, left, right, }) => {
                            let c = match operator {
                                Less => "<",
                                Equal => "=",
                                Greater => ">",
                                _ => unimplemented!("op {:?}", operator),
                            };

                            let i: i64 = match (left.borrow(), right.borrow()) {
                                (Base(Field(col)), Base(ConditionBase::Literal(Integer(i)))) => *i,
                                _ => unimplemented!("op condition {:?}", where_clause),
                            };

                            let filter = format!("{}{}", c, i);
                            s.create_filter(&filter).ok()
                        }
                        _ => unimplemented!("filter building {:?}", where_clause),
                    }
                },
                None => None,
            }
        };
        match query {
            CreateTable(CreateTableStatement{ table, fields, .. }) => {
                let Table { name, .. } = table;
                let schema = fields.iter()
                                .map(|col_spec: &ColumnSpecification| col_spec.column.name.clone())
                                .collect::<Vec<_>>();
                let schema = format!("({})", schema.join(","));
                println!("creating table {} ({})", name, schema);

                let mut file_path = self.base_path.clone();
                file_path.push(name);
                let file = OpenOptions::new()
                            .read(true).write(true).create(true)
                            .open(&file_path).unwrap();
                let mut s = Storage::create(file, &schema).unwrap();
                s.persist().expect("Storage failed to create table");

                self.tables.insert(name.to_string(), s);
            }
            Insert(InsertStatement{ table, data, ..}) => {
                println!("insert: \n{:?}\n{:?}", table, data);
                let mut s = self.tables.get_mut(&table.name).unwrap();
                for tuple in data {
                    let tuple = tuple.iter()
                                    .map(|literal| match literal {
                                        Integer(n) => format!("{}", n),
                                        others => panic!("unimplemented Literal type"),
                                    })
                                    .collect::<Vec<_>>();
                    let tuple = format!("({})", tuple.join(","));
                    let t = &mut Tuple::from_str(&s, &*tuple);
                    s.append(t);
                }
                s.persist().expect("Storage failed to insert Tuple");
            }
            Select(SelectStatement { tables, fields, where_clause, .. }) => {
                assert_eq!(1, tables.len(), "only one table at the time");
                //println!("{:?}\n{:?}", fields, where_clause);
                let s = self.tables.get(&tables[0].name).unwrap();
                let filter = mk_filter(&s, where_clause);

                println!("[");
                for entry in s.iter() {
                    let b = if let Some(filter) = &filter {
                            filter.matches(&entry)
                        } else {
                            true
                        };
                    if b {
                        println!("    {}", entry.to_string());
                    }
                }
                println!("]");
            },
            Delete(DeleteStatement { table, where_clause }) => {
                let mut s = self.tables.get_mut(&table.name).unwrap();
                let filter = mk_filter(&s, where_clause);

                let n = s.remove_all(&filter.unwrap());
                println!("Deleted {} rows", n);
            },
            //DropTable(DropTableStatement { tables, if_exists }) => {},
            Update(UpdateStatement { table, fields, where_clause }) => {
                println!("UpdateStatement {:?}\n{:?}\n{:?}", table, fields, where_clause);
                let mut s = self.tables.get_mut(&table.name).unwrap();
                let mut t = Tuple::from_str(&s, "");

                let n = s.update(&mut t).unwrap();
                println!("Updated {} rows", n);
            },
            _ => {
                println!("unimplemented query: {:?}", query);
            }
        }
    }
}


fn flush_stdout() {
    let stdout = io::stdout();
    let mut handle = stdout.lock();
    handle.flush().is_ok();
}

#[cfg(test)]
mod test {
    use super::*;
    use tempfile::{tempdir, TempDir};

    fn random_db() -> (TempDir, DataBase) {
        let dir = tempdir().unwrap();

        let db = DataBase::open_dir(dir.path().to_str().unwrap()).unwrap();

        (dir, db)
    }

    #[test]
    fn simple_create() {
        let (_dir, db) = random_db();
        let s = "CREATE TABLE blubb (u8, i8);";
    }

    #[test]
    fn simple_insert() {
        let s = "CREATE TABLE blubb (u8, i8);";
        let i = "INSERT INTO blubb VALUES (42, -23), (52, 13);";
    }

    #[test]
    fn delete() {
        let s = "CREATE TABLE blubb (u8, i8);";
        let i = "INSERT INTO blubb VALUES (42, -23), (52, 13);";
        let d = "DELETE FROM blubb WHERE `0` > 42;";
    }

    #[test]
    fn update() {
        let s = "CREATE TABLE blubb (u8, i8);";
        let i = "INSERT INTO blubb VALUES (42, -23), (52, 13);";
        let u = "UPDATE blubb SET `0` = 42, `1` = 18 WHERE `0` > 42;";
    }
}
