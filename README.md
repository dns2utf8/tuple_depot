# tuple_depot

An SQL interface to [tuple_storage](https://gitlab.com/dns2utf8/tuple_storage).

**State: PoC**

## Building from source

Clone this repo and make sure to have [rustup](https://rustup.rs/) installed then run this:

```bash
cargo run
```

## Example queries:

```sql
CREATE TABLE blubb (u8, i8);
INSERT INTO blubb VALUES (42, -23), (52, 13);
SELECT * FROM blubb;

-- Use with caution
UPDATE blubb SET `0` = 42, `1` = 18 WHERE `0` > 42;

DELETE FROM blubb WHERE `0` > 42;
```
